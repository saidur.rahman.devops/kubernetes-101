# Kubernetes RABC for USER

1. Generate a private key and a CSR file using the openssl command:

    ```
    openssl req -new -newkey rsa:2048 -nodes -keyout saidur.key -out saidur.csr -subj "/CN=saidur/O=mycompany"
    ```
2. Decrypt the key

    ```
    cat saidur.csr | base64 | tr -d "\n" > saidur-csr.yaml
    ```

3. Update CSR manifest-file
   ```
    apiVersion: certificates.k8s.io/v1
    kind: CertificateSigningRequest
    metadata:
      name: saidur
    spec:
      request: $(cat saidur.csr | base64 | tr -d '\n') #Decrypted key
      signerName: kubernetes.io/kube-apiserver-client
      #expirationSeconds: 86400  # one day
      usages:
      - client auth
    ```

4. Create CertificateSigningRequest

    ```
    kubectl apply -f saidur-csr.yaml
    ```

5. Generate CRT file

   ```
   kubectl get csr saidur -o jsonpath='{.status.certificate}'| base64 -d > saidur.crt
   ```

6. Create Role and Role Binding
   ```
   kubectl create role saidur-role --verb=create --verb=get --verb=list --verb=update --verb=delete --resource=pods --resource=deployments -oyaml --dry-run=client > saidur-role.yaml
   kubectl create rolebinding saidur-rolebind --role=saidur-role --user=saidur -oyaml --dry-run=client > saidur-rolebind.yaml
   kubectl apply -f saidur-role.yaml 
   kubectl apply -f saidur-rolebind.yaml
   ```
7. Create Config File for User "saidur" in saidur-config
   
   ```
   kubectl config set-credentials saidur --client-key=saidur.key --client-certificate=saidur.crt --embed-certs=true --kubeconfig=saidur-config
   kubectl config set-context rndcluster --cluster=kubernetes --user=saidur --kubeconfig=saidur-config
   cat .kube/config 
   ```

8. Add Cluster info from .kube/config in created config file and update current contexts
   #vim saidur-config 
   
   ```
    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority-data: 
        server: https://controlplane:6443
    name: kubernetes
    contexts:
    - context:
        cluster: kubernetes
        user: saidur
      name: rndcluster
    current-context: "rndcluster"
    kind: Config
    preferences: {}
    users:
    - name: saidur
      user:
        client-certificate-data: 
        client-key-data: 
   ```

9. Add User and Check the config
  ```
  adduser saidur
  su - saidur
  mkdir .kube
  touch .kube/config
  ```
