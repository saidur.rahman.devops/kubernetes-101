#!/bin/bash

sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

 sudo apt-get update
 sudo apt install docker-ce docker-ce-cli

sudo mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker

#

## Install Containerd

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt remove containerd
sudo apt update
sudo apt install containerd.io -y
sudo rm /etc/containerd/config.toml

sudo systemctl restart containerd

sudo systemctl enable containerd

# Installing kubeadm, kubelet and kubectl

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl

sudo apt-get install -y kubelet=1.26.2-00 kubectl=1.26.2-00 kubeadm=1.26.2-00
#sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl enable kubelet

sudo ufw disable
sudo swapoff -s
sudo swapoff -a
sudo sed -i '/swap/d' /etc/fstab


#Letting Iptables See Bridged Traffic:

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sudo sysctl --system

#sudo echo "192.168.0.101 k8s-master" >> /etc/hosts

#sudo echo "192.168.0.102 node01" >> /etc/hosts

#sudo echo "192.168.0.103 node02" >> /etc/hosts

echo "Installation Done !!"


##  On Master Node  ###

# kubeadm init --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address=10.100.10.11      #<controller node IP>  ## it  works for me



#mkdir -p $HOME/.kube
#sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#sudo chown $(id -u):$(id -g) $HOME/.kube/config

# kubectl get pod -A     #  make sure coredns is running

#sudo kubeadm init --v=5 --upload-certs --control-plane-endpoint 192.168.0.110:6443 --pod-network-cidr=10.244.0.0/16 --ignore-preflight-errors=NumCPU

## install weavenet CNI:

# kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml

## install flannel CNI:

# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

##Your Kubernetes control-plane has initialized successfully!

##To start using your cluster, you need to run the following as a regular user:

  #mkdir -p $HOME/.kube
  #sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  #sudo chown $(id -u):$(id -g) $HOME/.kube/config

##Alternatively, if you are the root user, you can run:

 #export KUBECONFIG=/etc/kubernetes/admin.conf
 
 
 ### Install autocompletion bash  ###
 
#sudo apt install bash-completion
 
#source <(kubectl completion bash)

#echo "source <(kubectl completion bash)" >> ~/.bashrc 
 
#alias k=kubectl
#complete -o default -F __start_kubectl k

#kubeadm token create --print-join-command

# calico: Following the calico Kubernetes documentation, we need to pass the flag --pod-network-cidr=192.168.0.0/16 to define the POD cidr network.
 
 

